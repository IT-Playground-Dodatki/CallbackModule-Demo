from ansible.plugins.callback import CallbackBase
from pprint import pprint


class CallbackModule(CallbackBase):

    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self):
        pass

    def v2_runner_on_ok(self, result):
        pprint("")
        pprint(result._result['ansible_facts']['moja_zmienna'])

    def v2_runner_on_failed(self, result, ignore_errors=False):
        pprint("Zadanie nieudane")


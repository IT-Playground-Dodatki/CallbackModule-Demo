from ansible.plugins.callback import CallbackBase
from pprint import pprint


class CallbackModule(CallbackBase):

    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self):
        pass

    def v2_runner_on_ok(self, result):
        file = open("inventory_1", "r")
        pprint(file.read())
        file.close()

    def v2_runner_on_failed(self, result, ignore_errors=False):
        pprint("Zadanie nieudane")


from ansible.plugins.callback import CallbackBase
from pprint import pprint


class CallbackModule(CallbackBase):

    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self):
        super(CallbackModule, self).__init__()
    pass

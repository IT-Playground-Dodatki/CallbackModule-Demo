from time import gmtime, strftime
from ansible.plugins.callback import CallbackBase
from pprint import pprint


class CallbackModule(CallbackBase):

    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self):
        self.start_time = strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())
        self.wypisz()

    def wypisz(self):
        pprint("-->  " + self.start_time)


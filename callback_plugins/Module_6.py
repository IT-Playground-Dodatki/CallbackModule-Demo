from ansible.plugins.callback import CallbackBase
from pprint import pprint


class CallbackModule(CallbackBase):

    CALLBACK_NEEDS_WHITELIST = True

    def __init__(self):
        pass

    def v2_runner_on_ok(self, result):
        pprint("Zadanie wykonane")

    def v2_runner_on_failed(self, result, ignore_errors=False):
        pprint("Zadanie nieudane")

